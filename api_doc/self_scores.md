# PUT /self_steps/:user_id controllers.SelfScoreController.upsert(user_id: Int)
ユーザの歩数を更新するAPI

## Example

### Request
```
PUT /self_steps/:user_id
```

### 正常系
#### Response
```
status 200
```

### 異常系
#### Response
```
status 500
```
