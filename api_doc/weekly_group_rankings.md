# GET /weekly_group_rankings/:user_id?ranking_sprint=2019/09/30
ranking_sprintの期間の指定ユーザのランキングを表示するAPI
ranking_sprintに指定がない場合は、指定ユーザの今日のランキングを取得する
## Example
### Request
```
GET /weekly_group_rankings/:user_id?ranking_sprint=2019/09/30
```

### 正常系
#### Response
```
status 200
Content-Type: application/json
{
    [
        "group_id": 1,
        "ranking_sprint": "2019/09/30",
        "weekly_group_scores": {
            user_id: 2,
            user_name: "テストさん"
            weekly_score: 123
        }
    ]
}
```

### 異常系
#### Response
```
status 500

status 400
```
