package repository

import java.util.{Calendar, Date}

import repository.weekly_group.WeeklyGroupRecord
import repository.user.UserRecord
import repository.weekly_score.WeeklyScoreRecord
import scalikejdbc.config.DBs
import scalikejdbc.{DBSession, NamedDB, insert, update, withSQL}
import scalikejdbc.interpolation.Implicits._

object SampleMigrateSpec {

  def main(args: Array[String]): Unit = {

    DBs.setupAll()

    NamedDB(Symbol("user_step_ranking")) localTx { implicit session =>
      generateTodaySamples()
      generateSampleBeforeToday()
    }
  }

  private def generateTodaySamples()(implicit session: DBSession): Unit = {
    val random = new scala.util.Random(10)

    print("START #generateTodaySamples")

    (0 to 99).foreach { i =>
      val groupId = insertSampleGroup().toInt
      (1 to 10).foreach { ii =>
        val userId = insertSampleUser(random.nextString(10)).toInt
        insertSampleScore(
          groupId = groupId,
          userId = userId,
          weeklyScore = random.nextInt(3),
          accumulatedScore = random.nextInt(10),
          wholeRank = i * 10 + ii
        )
      }
    }

    print("END #generateTodaySamples")
  }

  private def generateSampleBeforeToday()(implicit session: DBSession): Unit = {
    val random = new scala.util.Random(1000)

    print("START #generateSampleBeforeToday")

    (1 to 10).foreach { i =>
      (0 to 99).foreach { ii =>
        val groupId = insertSampleGroup().toInt
        updateSampleGroup(groupId, decreaseDateFromToday(ii + i))
        (1 to 10).foreach { iii =>
          insertSampleScore(
            groupId = groupId,
            userId = ii * 10 + iii,
            weeklyScore = random.nextInt(1000),
            accumulatedScore = random.nextInt(100000),
            wholeRank = ii * 10 + iii
          )
        }
      }
    }

    print("END #generateSampleBeforeToday")
  }

  private def insertSampleUser(
    name: String
  )(implicit session: DBSession): Long = {

    import UserRecord.column
    withSQL {
      insert.into(UserRecord).namedValues(column.name -> name)
    }.updateAndReturnGeneratedKey().apply()
  }

  private def insertSampleGroup()(implicit session: DBSession): Long = {

    import repository.weekly_group.WeeklyGroupRecord.column
    withSQL {
      insert
        .into(WeeklyGroupRecord)
        .namedValues(column.created_at -> new Date())
    }.updateAndReturnGeneratedKey().apply()
  }

  private def updateSampleGroup(id: Int, targetDate: Date)(
    implicit session: DBSession
  ): Unit = {

    import repository.weekly_group.WeeklyGroupRecord.column
    withSQL {
      update(WeeklyGroupRecord)
        .set(column.created_at -> targetDate)
        .where
        .eq(column.id, id)
    }.update().apply()
  }

  private def decreaseDateFromToday(amount: Int) = {
    val calendar = Calendar.getInstance()
    calendar.setTime(new Date());
    calendar.add(Calendar.DATE, -amount)
    calendar.getTime
  }

  private def insertSampleScore(
    groupId: Int,
    userId: Int,
    weeklyScore: Int,
    accumulatedScore: Int,
    wholeRank: Int
  )(implicit session: DBSession): Unit = {

    import repository.weekly_score.WeeklyScoreRecord.column
    val valueMap = Map(
      column.group_id -> groupId,
      column.user_id -> userId,
      column.weekly_score -> weeklyScore,
      column.accumulated_score -> accumulatedScore,
      column.whole_rank -> wholeRank,
    )

    withSQL {
      insert.into(WeeklyScoreRecord).namedValues(valueMap)
    }.update.apply()
  }
}
