package repository

import scalikejdbc.NamedDB
import scalikejdbc.config.DBs
import scalikejdbc.interpolation.Implicits._
import tasks.WeeklyScoreSyncTask

/**
  * 手元で実装を簡単に確かめるためのスクリプト
  */
object TestSpec {

  def main(args: Array[String]): Unit = {
    DBs.setupAll()

    println("start")
    WeeklyScoreSyncTask.exec()
    println("end")
  }

}
