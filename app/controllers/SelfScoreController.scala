package controllers

import domain.user_score.UserScoreUpserter
import javax.inject._
import play.api.Configuration
import play.api.mvc.{
  AbstractController,
  AnyContent,
  ControllerComponents,
  Request
}
import scalikejdbc.NamedDB
import scalikejdbc.interpolation.Implicits._

@Singleton
class SelfScoreController @Inject()(cc: ControllerComponents,
                                    val config: Configuration)
    extends AbstractController(cc) {

  def upsert(user_id: Int) =
    Action { implicit request: Request[AnyContent] =>
      NamedDB(Symbol("user_step_ranking")) localTx { implicit session =>
        new UserScoreUpserter(user_id).upsert()
      } match {
        case Right(_) => new Status(OK)
        case Left(_)  => InternalServerError
      }
    }
}
