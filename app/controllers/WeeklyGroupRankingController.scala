package controllers

import domain.weekly_score.WeeklyGroupRankingLoader
import javax.inject._
import model.{IllegalRequest, WeeklyScoreSelectParameter}
import play.api.Configuration
import play.api.libs.json.Json
import play.api.mvc._
import scalikejdbc.NamedDB
import scalikejdbc.interpolation.Implicits._

@Singleton
class WeeklyGroupRankingController @Inject()(cc: ControllerComponents,
                                             val config: Configuration)
    extends AbstractController(cc) {

  def index(user_id: Int, ranking_sprint: Option[String]) = Action {
    implicit request: Request[AnyContent] =>
      new WeeklyScoreSelectParameter(ranking_sprint).validate.flatMap { param =>
        NamedDB(Symbol("user_step_ranking")) readOnly { implicit session =>
          new WeeklyGroupRankingLoader(param)
            .loadByUserId(user_id)
        }
      } match {
        case Right(weeklyGroupScores) => Ok(Json.toJson(weeklyGroupScores))
        case Left(_: IllegalRequest)  => BadRequest
        case Left(_)                  => InternalServerError
      }
  }
}
