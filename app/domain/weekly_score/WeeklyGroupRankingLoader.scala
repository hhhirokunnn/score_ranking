package domain.weekly_score

import domain.core.{DomainError, WeeklyScoreLoaderError}
import model.{WeeklyGroupRanking, WeeklyScoreSelectParameter}
import repository.weekly_group_ranking.WeeklyGroupRankingSelector
import scalikejdbc.DBSession

import scala.util.Try

class WeeklyGroupRankingLoader(selectParameter: WeeklyScoreSelectParameter) {

  def loadByUserId(user_id: Int)(
    implicit session: DBSession
  ): Either[DomainError, Seq[WeeklyGroupRanking]] = {
    Try {
      new WeeklyGroupRankingSelector(selectParameter)
        .selectByUserId(user_id)
    }.toEither match {
      case Left(e: Exception) => Left(WeeklyScoreLoaderError(e))
      case Right(v)           => Right(v)
    }
  }
}
