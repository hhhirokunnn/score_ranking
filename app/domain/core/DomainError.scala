package domain.core

trait DomainError extends Exception

case class WeeklyScoreLoaderError(exception: Exception) extends DomainError
case class UserScoreUpserterError(exception: Exception) extends DomainError
