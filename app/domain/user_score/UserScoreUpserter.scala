package domain.user_score

import domain.core.{DomainError, UserScoreUpserterError}
import repository.user_score.UserScoreUpdateInserter
import scalikejdbc.DBSession

import scala.util.Try

class UserScoreUpserter(user_id: Int) {

  def upsert()(implicit session: DBSession): Either[DomainError, Unit] =
    Try {
      new UserScoreUpdateInserter(user_id).upsert()
    }.toEither match {
      case Left(e: Exception) => Left(UserScoreUpserterError(e))
      case Right(_)           => Right()
    }

}
