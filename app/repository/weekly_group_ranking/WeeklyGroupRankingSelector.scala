package repository.weekly_group_ranking

import java.text.SimpleDateFormat

import model.WeeklyGroupRanking.WeeklyScore
import model.{WeeklyGroupRanking, WeeklyScoreSelectParameter}
import repository.user.UserRecord
import repository.user.UserRecord.u
import repository.user_score.UserScoreRecord
import repository.user_score.UserScoreRecord.us
import repository.weekly_group.WeeklyGroupRecord
import repository.weekly_group.WeeklyGroupRecord.wg
import repository.weekly_score.WeeklyScoreRecord
import repository.weekly_score.WeeklyScoreRecord.ws
import scalikejdbc.interpolation.Implicits._
import scalikejdbc.{DBSession, WrappedResultSet, select, withSQL}

class WeeklyGroupRankingSelector(selectParameter: WeeklyScoreSelectParameter) {

  def selectByUserId(
    user_id: Int
  )(implicit session: DBSession): Seq[WeeklyGroupRanking] = {

    val DateFormat = new SimpleDateFormat("yyyy/MM/dd")

    def weeklyScore(set: WrappedResultSet) =
      if (selectParameter.isToday)
        set.int(us.resultName.score) - set.int(ws.resultName.accumulated_score)
      else set.int(ws.resultName.weekly_score)

    val * = (set: WrappedResultSet) =>
      WeeklyGroupRanking(
        group_id = set.int(ws.resultName.group_id),
        ranking_sprint = DateFormat.format(set.date(wg.resultName.created_at)),
        weekly_group_scores = WeeklyScore(
          user_id = set.int(ws.resultName.user_id),
          user_name = set.string(u.resultName.name),
          weekly_score = weeklyScore(set),
        )
    )

    withSQL {
      select
        .from(WeeklyScoreRecord as ws)
        .join(UserRecord as u)
        .on(u.id, ws.user_id)
        .join(WeeklyGroupRecord as wg)
        .on(wg.id, ws.group_id)
        .join(UserScoreRecord as us)
        .on(u.id, us.user_id)
        .where
        .append(sqls"""
               wg.id in (
                 select ws.group_id from users u
                 join weekly_scores ws on ws.user_id = u.id
                 join weekly_groups wg on wg.id = ws.group_id
                 where u.id = $user_id
                 and cast(wg.created_at as date) = ${selectParameter.targetSprint}
                )""")
    }.map(*).list.apply()
  }
}
