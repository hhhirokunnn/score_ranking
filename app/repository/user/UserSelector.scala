package repository.user

import repository.user.UserRecord.u
import scalikejdbc.{DBSession, select, withSQL}
import scalikejdbc.interpolation.Implicits._

class UserSelector {

  def selectById(id: Int)(implicit session: DBSession): Seq[UserRecord] = {
    withSQL {
      select
        .from(UserRecord as u)
        .where
        .eq(u.id, id)
    }.map(UserRecord.*).list.apply()
  }
}
