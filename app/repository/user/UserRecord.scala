package repository.user

import java.util.Date

import repository.core.UserStepRankingSyntaxSupport
import scalikejdbc.{SQLSyntaxSupport, WrappedResultSet}

/**
  * ユーザのクラス
  * @param id ユーザID
  * @param name ユーザ名
  * @param created_at ユーザの作成日
  */
case class UserRecord(id: Int, name: String, created_at: Date)

object UserRecord extends UserStepRankingSyntaxSupport[UserRecord]("users") {

  val u: Syntax = UserRecord.syntax("u")

  val * : WrappedResultSet => UserRecord = set =>
    UserRecord(
      id = set.int(u.resultName.id),
      name = set.string(u.resultName.name),
      created_at = set.date(u.resultName.created_at),
  )
}
