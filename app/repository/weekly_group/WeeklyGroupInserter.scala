package repository.weekly_group

import java.util.Date

import repository.weekly_score.WeeklyScoreRecord
import scalikejdbc.{DBSession, NamedDB, withSQL}

class WeeklyGroupInserter {

  def insert()(implicit session: DBSession) = {

    val column = WeeklyGroupRecord.column

    NamedDB(Symbol("user_step_ranking")) localTx { implicit session =>
      withSQL {
        scalikejdbc.insert
          .into(WeeklyScoreRecord)
          .namedValues(column.created_at -> new Date())
      }.update.apply()
    }
  }
}
