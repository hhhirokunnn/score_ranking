package repository.weekly_group

import repository.weekly_group.WeeklyGroupRecord.wg
import scalikejdbc.interpolation.Implicits._
import scalikejdbc.interpolation.SQLSyntax
import scalikejdbc.{DBSession, select, withSQL}

class WeeklyGroupSelector {

  /**
    * 本日作成されたgroupをactiveGroupとする。
    */
  def selectActiveGroups()(
    implicit session: DBSession
  ): Seq[WeeklyGroupRecord] =
    withSQL {
      select
        .from(WeeklyGroupRecord as wg)
        .where
        .eq(sqls"cast(${wg.created_at} as date)", SQLSyntax.currentDate)
    }.map(WeeklyGroupRecord.*).list.apply()
}
