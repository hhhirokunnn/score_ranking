package repository.weekly_group

import java.util.Date

import repository.core.UserStepRankingSyntaxSupport
import scalikejdbc.{SQLSyntaxSupport, WrappedResultSet}

/**
  * @param id グループID
  * @param created_at グループ作成日(active_term)
  */
case class WeeklyGroupRecord(id: Int, created_at: Date)

object WeeklyGroupRecord
    extends UserStepRankingSyntaxSupport[WeeklyGroupRecord]("weekly_groups") {

  val wg: Syntax = WeeklyGroupRecord.syntax("wg")

  val * : WrappedResultSet => WeeklyGroupRecord = set =>
    WeeklyGroupRecord(
      id = set.int(wg.id),
      created_at = set.date(wg.created_at),
  )
}
