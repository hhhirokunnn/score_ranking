package repository.user_score

import repository.core.UserStepRankingSyntaxSupport
import scalikejdbc.{SQLSyntaxSupport, WrappedResultSet}

/**
  * ユーザとそのユーザのリアルタイムの累積スコアを扱うクラス
  * @param user_id ユーザID
  * @param score ユーザの累積スコア
  */
case class UserScoreRecord(user_id: Int, score: Int)

object UserScoreRecord
    extends UserStepRankingSyntaxSupport[UserScoreRecord]("user_scores") {

  val us: Syntax = UserScoreRecord.syntax("us")

  val * : WrappedResultSet => UserScoreRecord = set =>
    UserScoreRecord(
      user_id = set.int(us.resultName.user_id),
      score = set.int(us.resultName.score),
  )
}
