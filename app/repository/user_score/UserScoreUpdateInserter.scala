package repository.user_score

import scalikejdbc.interpolation.Implicits._
import scalikejdbc.{DBSession, insert, withSQL}

class UserScoreUpdateInserter(user_id: Int) {

  def upsert()(implicit session: DBSession): Unit = {
    val column = UserScoreRecord.column
    withSQL {
      insert
        .into(UserScoreRecord)
        .namedValues(
          column.user_id -> user_id,
          column.score -> UpdateScoreValue
        )
        .append(sqls"on duplicate key update score = $UpdateScoreValue")
    }.update.apply()
  }

  // 頻度がたかそうなので、既存のデータの歩数を確認するクエリを走らせないためにサブクエリにします。
  lazy val UpdateScoreValue =
    sqls"""(select ifnull(tmp.score, 1) as score
          from (
          select sum(score+1) as score
          from user_scores
          where user_id = $user_id
          ) as tmp)
       """
}
