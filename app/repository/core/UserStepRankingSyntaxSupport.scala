package repository.core
import scalikejdbc.SQLSyntaxSupport

/**
  * ScalikeJDBCの諸設定を共通化するためのクラス
  */
class UserStepRankingSyntaxSupport[A](table: String)
    extends SQLSyntaxSupport[A] {
  override val useSnakeCaseColumnName = false
  override val connectionPoolName = Symbol("user_step_ranking")
  override val tableName = table

  type Syntax = scalikejdbc.SyntaxProvider[A]
}
