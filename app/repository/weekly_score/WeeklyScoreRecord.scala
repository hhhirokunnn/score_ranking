package repository.weekly_score

import repository.core.UserStepRankingSyntaxSupport
import scalikejdbc.{SQLSyntaxSupport, WrappedResultSet}

/**
  * @param id
  * @param group_id グループID
  * @param user_id　ユーザID
  * @param weekly_score グループに所属していた日の歩数(今日の歩数の同期は翌日に行います。)
  * @param accumulated_score ユーザの累積歩数
  * @param whole_rank グループに所属していた日のユーザ全体のランキング
  */
case class WeeklyScoreRecord(id: Int,
                             group_id: Int,
                             user_id: Int,
                             weekly_score: Int,
                             accumulated_score: Int,
                             whole_rank: Int)

object WeeklyScoreRecord
    extends UserStepRankingSyntaxSupport[WeeklyScoreRecord]("weekly_scores") {

  val ws: Syntax = WeeklyScoreRecord.syntax("ws")

  val * : WrappedResultSet => WeeklyScoreRecord = set =>
    WeeklyScoreRecord(
      id = set.int(ws.resultName.id),
      group_id = set.int(ws.resultName.group_id),
      user_id = set.int(ws.resultName.user_id),
      weekly_score = set.int(ws.resultName.weekly_score),
      accumulated_score = set.int(ws.resultName.accumulated_score),
      whole_rank = set.int(ws.resultName.whole_rank),
  )
}
