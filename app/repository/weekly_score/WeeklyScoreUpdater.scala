package repository.weekly_score

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import repository.user_score.UserScoreRecord
import repository.weekly_group.WeeklyGroupRecord
import repository.weekly_score.WeeklyScoreRecord.ws
import scalikejdbc.interpolation.Implicits._
import scalikejdbc.{DBSession, NamedDB, withSQL}
import repository.user_score.UserScoreRecord.us
import repository.weekly_group.WeeklyGroupRecord.wg

class WeeklyScoreUpdater {

  /**
    *  一日の歩数をweekly_scoresテーブルに同期するための更新ロジック
    * @param session
    */
  def updateDayResult()(implicit session: DBSession): Unit = {

    def dateFromTodayBefore(gap: Int) = {
      val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
      val calendar = Calendar.getInstance()
      calendar.setTime(new Date());
      calendar.add(Calendar.DATE, -gap)
      dateFormat.format(calendar.getTime)
    }

    val userRanks = withSQL {
      scalikejdbc
        .select(us.user_id, us.score, sqls"@i:=@i+1 AS rank")
        .from(UserScoreRecord as us)
        .append(sqls"JOIN (SELECT @i:=0) ranking ON 1 = 1")
        .join(WeeklyScoreRecord as ws)
        .on(ws.user_id, us.user_id)
        .join(WeeklyGroupRecord as wg)
        .on(wg.id, ws.group_id)
        .where
        .eq(sqls"CAST(wg.created_at AS DATE)", dateFromTodayBefore(1))
        .orderBy(us.score)
        .desc
    }.map { res =>
        Map(
          "user_id" -> res.int("user_id"),
          "score" -> res.int("score"),
          "rank" -> res.int("rank"),
        )
      }
      .list()
      .apply()

    userRanks.foreach { userRank =>
      withSQL {
        scalikejdbc
          .update(WeeklyScoreRecord as ws)
          .set(
            ws.accumulated_score -> userRank.get("score"),
            ws.whole_rank -> userRank.get("rank")
          )
          .where
          .eq(ws.user_id, userRank.get("user_id"))
      }.update().apply()
    }

    val weeklyScores =
      withSQL {
        scalikejdbc
          .select(
            us.user_id,
            sqls"us.score - ws.accumulated_score as weekly_score"
          )
          .from(UserScoreRecord as us)
          .join(WeeklyScoreRecord as ws)
          .on(us.user_id, ws.user_id)
          .join(WeeklyGroupRecord as wg)
          .on(wg.id, ws.group_id)
          .where
          .eq(sqls"CAST(wg.created_at AS DATE)", dateFromTodayBefore(1))
      }.map { res =>
          Map(
            "user_id" -> res.int("user_id"),
            "weekly_score" -> res.int("weekly_score")
          )
        }
        .list
        .apply()

    weeklyScores.map { weeklyScore =>
      withSQL {
        scalikejdbc
          .update(WeeklyScoreRecord as ws)
          .set(ws.weekly_score -> weeklyScore.get("weekly_score"))
          .where
          .eq(ws.user_id, weeklyScore.get("user_id"))
      }.update().apply()
    }
  }
}
