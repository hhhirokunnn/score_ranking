package tasks.core

import akka.actor.Actor
import tasks.WeeklyScoreSyncTask

class UserStepActor extends Actor {
  override def receive = {
    case WeeklyScoreSyncTask => WeeklyScoreSyncTask.exec()
  }
}
