package tasks

import repository.weekly_score.WeeklyScoreUpdater
import scalikejdbc.NamedDB

sealed trait UserStepTask

/**
  * 一日の歩数をweekly_scoresテーブルに同期するためのバッチ
  */
case class WeeklyScoreSyncTask() extends UserStepTask {}

object WeeklyScoreSyncTask {
  def exec() = NamedDB(Symbol("user_step_ranking")) localTx {
    implicit session =>
      new WeeklyScoreUpdater().updateDayResult()
  }
}
