package tasks

import java.util.concurrent.TimeUnit.SECONDS

import akka.actor.Props
import scalikejdbc.NamedDB
import tasks.core.UserStepActor

import scala.concurrent.duration.{DAYS, FiniteDuration}

object TaskScheduler {

  def main(args: Array[String]): Unit = {

    val system = akka.actor.ActorSystem("system")
    val actor = system.actorOf(Props(new UserStepActor))

    import system.dispatcher

    system.scheduler.schedule(
      FiniteDuration(0, SECONDS),
      FiniteDuration(1, DAYS),
      actor,
      WeeklyScoreSyncTask
    )
  }
}
