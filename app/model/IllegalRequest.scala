package model

trait IllegalRequest extends Exception

sealed trait WeeklyScoreSelectParameterError extends IllegalRequest
case class DateFormatError(e: Exception) extends WeeklyScoreSelectParameterError
