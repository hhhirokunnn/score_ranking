package model

import java.text.SimpleDateFormat
import java.util.Date

import scala.util.Try

class WeeklyScoreSelectParameter(ranking_sprint: Option[String]) {

  private lazy val DateFormat = new SimpleDateFormat("yyyy/MM/dd")
  private val Today: String = DateFormat.format(new Date)

  lazy val isToday: Boolean = Today == targetSprint

  lazy val targetSprint: String = ranking_sprint.getOrElse(Today)

  def validate
    : Either[WeeklyScoreSelectParameterError, WeeklyScoreSelectParameter] = {
    Try {
      ranking_sprint.map(DateFormat.parse)
    }.toEither match {
      case Left(e: Exception) => Left(DateFormatError(e))
      case _                  => Right(this)
    }
  }
}
