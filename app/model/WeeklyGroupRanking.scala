package model

import model.WeeklyGroupRanking.WeeklyScore
import play.api.libs.json.{Json, Writes}

case class WeeklyGroupRanking(group_id: Int,
                              ranking_sprint: String,
                              weekly_group_scores: WeeklyScore)

object WeeklyGroupRanking {
  implicit val writes: Writes[WeeklyGroupRanking] =
    Json.writes[WeeklyGroupRanking]

  case class WeeklyScore(user_id: Int, user_name: String, weekly_score: Int)
  object WeeklyScore {
    implicit val writes: Writes[WeeklyScore] =
      Json.writes[WeeklyScore]
  }
}
