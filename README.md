# user_step_ranking

## API定義

* [歩数更新API](api_doc/self_scores.md)
* [ランキング表示API](api_doc/self_scores.md)

## テーブル定義


### table名: users

```
/**
  * ユーザのクラス
  * @param id ユーザID
  * @param name ユーザ名
  * @param created_at ユーザの作成日
  */
case class UserRecord(id: Int, name: String, created_at: Date)
```

### table名: user_scores

```
/**
  * ユーザとそのユーザのリアルタイムの累積スコアを扱うクラス
  * @param user_id ユーザID
  * @param score ユーザの累積スコア
  */
case class UserScoreRecord(user_id: Int, score: Int)
```

### table名: weekly_groups

```
/**
  * @param id グループID
  * @param created_at グループ作成日(active_term)
  */
case class WeeklyGroupRecord(id: Int, created_at: Date)
```

### table名: weekly_scores

```
/**
  * @param id
  * @param group_id グループID
  * @param user_id　ユーザID
  * @param weekly_score グループに所属していた日の歩数(今日の歩数の同期は翌日に行います。)
  * @param accumulated_score ユーザの累積歩数
  * @param whole_rank グループに所属していた日のユーザ全体のランキング
  */
case class WeeklyScoreRecord(id: Int,
                             group_id: Int,
                             user_id: Int,
                             weekly_score: Int,
                             accumulated_score: Int,
                             whole_rank: Int)
```
