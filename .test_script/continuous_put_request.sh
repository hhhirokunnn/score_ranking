#!/bin/sh

# Example
#   sh continuous_put_request.sh http://localhost:8080/webapi/user/XXXXX 10 '{"token":"xxxxx","version":"1.3"}'
#

URL=http://localhost:9000/self_step/
INDEX=200

for ((i=0 ; i<2000 ; i++))
do
  for ((n=1 ; n<INDEX+1 ; n++))
  do
    curl -XPUT $URL$n
  done
done
