-- !Ups
CREATE TABLE `users` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(20) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);
# --- !Downs
DROP TABLE users;

-- !Ups
CREATE TABLE `user_scores` (
    `id` BIGINT(20) NOT NULL UNIQUE AUTO_INCREMENT,
    `user_id` BIGINT(20) NOT NULL,
    `score` BIGINT(20) NOT NULL DEFAULT 0,
    PRIMARY KEY (user_id)
);
# --- !Downs
DROP TABLE user_scores;

-- !Ups
CREATE TABLE `weekly_groups` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    INDEX(created_at)
);
# --- !Downs
DROP TABLE weekly_groups;

-- !Ups
CREATE TABLE `weekly_scores` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `group_id` BIGINT(20) NOT NULL,
    `user_id` BIGINT(20) NOT NULL,
    `weekly_score` BIGINT(20) NOT NULL DEFAULT 0, -- その週のスコア
    `accumulated_score` BIGINT(20) NOT NULL DEFAULT 0, -- その週までの累積スコア
    `whole_rank` BIGINT(20) NOT NULL DEFAULT 0,
    PRIMARY KEY (id),
    INDEX(group_id),
    INDEX(user_id)
);
# --- !Downs
DROP TABLE weekly_scores;
